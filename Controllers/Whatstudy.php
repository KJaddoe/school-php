<?php
class Whatstudy extends Controller {
    public static function checkChatUser($id) {
        if (!empty($id)) {
            $params = array(
                ":id" => $id
            );
            $results = self::query("SELECT * FROM chat_user where userID = :id", $params);
        } else {

        }

        return $results;
    }

    public static function getChatRoles() {
        $results = self::query("SELECT * FROM chat_role");

        return $results;
    }

    public static function registerChatUser($id, $role) {
        if (!empty($id) && !empty($role)) {
            $params = array(
                ":id" => $id,
                ":role" => $role
            );
            $results = self::query("INSERT INTO chat_user (userID, chat_roleID) VALUES (:id, :role)", $params);

            $response= array(
                "success" => "Your account has been registered succesfully"
            );
        } else {
            $response= array(
                "error" => "please fill in all required fields!!!"
            );
        }

        return json_encode($response);
    }

    public static function getUserChats($id) {
        $params = array(
            ":id" => $id
        );
        $results = self::query("SELECT * FROM chat_view where userID = :id", $params);

        return $results;
    }

    public static function getChat($id) {
        $params = array(
            ":id" => $id
        );
        $results = self::query("SELECT * FROM user_chat where chatID = :id order by dateSend", $params);

        return $results;
    }

    public static function sendChatMessage($userID, $message, $chatID) {
        $params = array(
            ":userID" => $userID,
            ":message" => $message,
            ":chatID" => $chatID
        );
        $results = self::query("
            INSERT INTO message (userID, message) VALUES (:userID, :message);
            INSERT INTO message_in_chat (chatID, messageID) VALUES (:chatID, (select ID from message where userID = :userID and sendDate = CURRENT_TIMESTAMP));
        ", $params);

        return $results;
    }

    public static function createNewChat($userID, $user2ID) {
        $params = array(
            ":userID" => $userID,
            ":user2ID" => $user2ID,
        );
        $results = self::query("
            INSERT INTO chat (user1, user2) VALUES (:userID, :user2ID);
        ", $params);

        return $results;
    }
}
?>
