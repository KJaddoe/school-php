<?php
class TicTacToe extends Controller {
    private $turn;
    private $player;
    private $gameOver;
    private $winner;
    private $score;
    private $game;

    public function __construct() {
        $this->turn = rand(0,1);
        $this->player = array("human","computer");
        $this->gameOver = false;
        $this->winner = "draw";
        $this->score = array(
            $this->player[0] => 0,
            $this->player[1] => 0,
        );
        $this->game = array("","","","","","","","","");


        if (!isset($_SESSION['turn']) || !isset($_SESSION['gameOver']) || !isset($_SESSION['winner']) || !isset($_SESSION['score']) || !isset($_SESSION['game'])) {
            $_SESSION['turn'] = $this->turn;
            $_SESSION['gameOver'] = $this->gameOver;
            $_SESSION['winner'] = $this->winner;
            $_SESSION['score'] = $this->score;
            $_SESSION['game'] = $this->game;
        } else {
            $this->turn = $_SESSION['turn'];
            $this->gameOver = $_SESSION['gameOver'];
            $this->winner = $_SESSION['winner'];
            $this->score = $_SESSION['score'];
            $this->game = $_SESSION['game'];
        }

        $_SESSION['player'] = $this->setPlayer($_SESSION['turn']);
    }

    private function setPlayer($turn) {
        $player = $this->player[$turn];

        return $player;
    }

    public function changeTurn() {
        switch ($this->turn) {
            case 1:
                $_SESSION['turn'] = 0;
                break;
            case 0:
                $_SESSION['turn'] = 1;
                break;
        }

        $_SESSION['player'] = $this->setPlayer($this->turn);
    }

    public function computerTurn($player, $gameOver, $game) {
        if ($player === "computer" && $gameOver === false && $this->winner === "draw") {
            if ($game == ["","","","","","","","",""]) {
                $i = 0;
            } else if ($game == ["-1","","1","","","","","",""] || $game == ["-1","","","","","","1","",""] || $game == ["-1","","","","","","","","1"] || $game == ["1","","","","","","","",""] || $game == ["","","1","","","","","",""] || $game == ["","","","","","","1","",""] || $game == ["","","","","","","","","1"]) {
                $i = 4;
            } else if ($game == ["-1","","","","1","","","",""]) {
                $i = 2;
            } else if ($game == ["1","","1","","-1","","","",""]) {
                $i = 1;
            } else if ($game == ["","","1","","-1","","","","1"]) {
                $i = 5;
            } else if ($game == ["","","","","-1","","1","","1"]) {
                $i = 7;
            } else if ($game == ["1","","","","-1","","1","",""]) {
                $i = 3;
            } else if ($game == ["-1","1","-1","","1","","","",""]) {
                $i = 7;
            } else if ($game == ["-1","1","-1","1","1","","","-1",""]) {
                $i = 5;
            } else if ($game == ["-1","1","-1","","1","1","","-1",""]) {
                $i = 3;
            } else if ($game == ["","","1","","-1","-1","","","1"]) {
                $i = 3;
            } else if ($game == ["","","","","-1","","1","-1","1"]) {
                $i = 1;
            } else if ($game == ["1","","","-1","-1","","1","",""]) {
                $i = 5;
            } else if ($game[2] === "1" && $game[8] === "1" && $game[5] === "") {
                $i = 5;
            } else if ($game[2] === "1" && $game[6] === "1" && $game[3] === "") {
                $i = 3;
            } else if ($game[6] === "1" && $game[7] === "1" && $game[8] === "") {
                $i = 8;
            } else if ($game[0] === "1" && $game[4] === "1" && $game[8] === "") {
                $i = 8;
            } else if ($game[0] === "1" && $game[3] === "1" && $game[6] === "") {
                $i = 6;
            } else if ($game[2] === "1" && $game[5] === "1" && $game[8] === "") {
                $i = 8;
            } else if ($game[3] === "1" && $game[4] === "1" && $game[5] === "") {
                $i = 5;
            } else if ($game[5] === "1" && $game[4] === "1" && $game[3] === "") {
                $i = 3;
            } else if ($game[0] === "1" && $game[2] === "1" && $game[1] === "") {
                $i = 1;
            } else if ($game[6] === "1" && $game[8] === "1" && $game[7] === "") {
                $i = 7;
            } else if ($game[4] === "1" && $game[7] === "1" && $game[1] === "") {
                $i = 1;
            } else if ($game[1] === "1" && $game[4] === "1" && $game[7] === "") {
                $i = 7;
            } else if ($game[8] === "1" && $game[4] === "1" && $game[0] === "") {
                $i = 0;
            } else if ($game[2] === "1" && $game[4] === "1" && $game[6] === "") {
                $i = 6;
            } else if ($game[6] === "1" && $game[4] === "1" && $game[2] === "") {
                $i = 2;
            } else if ($game[3] === "1" && $game[5] === "1" && $game[4] === "") {
                $i = 4;
            } else if ($game[5] === "1" && $game[8] === "1" && $game[2] === "") {
                $i = 2;
            } else if ($game[8] === "1" && $game[7] === "1" && $game[6] === "") {
                $i = 6;
            } else if ($game[6] === "1" && $game[3] === "1" && $game[0] === "") {
                $i = 0;
            } else if ($game[1] === "1" && $game[7] === "1" && $game[4] === "") {
                $i = 4;
            } else if ($game[0] === "-1" && $game[2] === "-1" && $game[1] === "") {
                $i = 1;
            } else if ($game == ["1","-1","1","1","-1","","","",""] || $game == ["1","-1","1","","-1","1","","",""] || $game == ["1","-1","1","","-1","","1","",""] || $game == ["1","-1","1","","-1","","","","1"]) {
                $i = 7;
            } else if ($game == ["1","-1","1","1","-1","","","",""] || $game == ["1","-1","1","","-1","1","","",""] || $game == ["1","-1","1","","-1","","1","",""] || $game == ["1","-1","1","","-1","","","","1"]) {
                $i = 7;
            } else {
                for ($i=0; $i<=8; $i++) {
                    if ($game[$i] == 0) {
                        $blank = 1;
                    }
                }

                if ($blank == 1) {
                    $i = rand(0,8);
                    if ($game[0] == 1 && $game[1] == 1 && $game[2] != -1) {
                        $i = 2;
                    }
                    while ($game[$i] != 0){
                        $i = rand(0,8);
                    }
                }
            }

            $game[$i] = -1;

            $_SESSION['game'] = $game;
            $this->changeTurn();
            $this->checkWinner();

            header("Refresh: 0;");
            //return "<script>window.location = window.location.href;</script>";
        }
    }

    public function checkWinner() {
        $game = $this->game;
        $message = "";

        if ($this->gameOver == false) {
            if ( $game[0]==1 && $game[1]==1 && $game[2]==1 || $game[3]==1 && $game[4]==1 && $game[5]==1 || $game[6]==1 && $game[7]==1 && $game[8]==1 || $game[0]==1 && $game[3]==1 && $game[6]==1 || $game[1]==1 && $game[4]==1 && $game[7]==1 || $game[2]==1 && $game[5]==1 && $game[8]==1 || $game[0]==1 && $game[4]==1 && $game[8]==1 || $game[2]==1 && $game[4]==1 && $game[6]==1 ) {
                $_SESSION['winner'] = "human";
                $_SESSION['gameOver'] = true;
                $_SESSION['score']['human']++;
                $message = "<script>window.location = window.location.href;</script>";
            } else if($game[0]==-1 && $game[1]==-1 && $game[2]==-1 || $game[3]==-1 && $game[4]==-1 && $game[5]==-1 || $game[6]==-1 && $game[7]==-1 && $game[8]==-1 || $game[0]==-1 && $game[3]==-1 && $game[6]==-1 || $game[1]==-1 && $game[4]==-1 && $game[7]==-1 || $game[2]==-1 && $game[5]==-1 && $game[8]==-1 || $game[0]==-1 && $game[4]==-1 && $game[8]==-1 || $game[2]==-1 && $game[4]==-1 && $game[6]==-1 ) {
                $_SESSION['winner'] = "computer";
                $_SESSION['gameOver'] = true;
                $_SESSION['score']['computer']++;
                $message = "<script>window.location = window.location.href;</script>";
            } else if (!in_array("", $game)) {
                $_SESSION['winner'] = "draw";
                $_SESSION['gameOver'] = true;
                $message = "<script>window.location = window.location.href;</script>";
            }
        }
        return $message;
    }

    public function resetBoard() {
        $_SESSION['game'] = array("","","","","","","","","");
        $_SESSION['turn'] = rand(0,1);
        $_SESSION['gameOver'] = false;
        $_SESSION['player'] = $this->setPlayer($_SESSION['turn']);
        $_SESSION['winner'] = "draw";
    }

    public function resetGame() {
        session_unset();
        session_destroy();
    }
}

?>
