<?php
class View {
    private $data = array();

    private $render = FALSE;

    public function __construct($controller,$template) {
        try {
            $file = './Views/' . $controller .'/' . $template . '.php';

            if (file_exists($file)) {
                $this->render = $file;
            } else {
                throw new customException('Template ' . $template . ' not found!');
            }
        } catch (customException $e) {
            echo $e->errorMessage();
        }
    }

    public function assign($variable, $value) {
            $this->data[$variable] = $value;
    }

    public function __destruct() {
        extract($this->data);
        require_once("./Views/_Layout/menu.php");
        require_once($this->render);
        require_once("./Views/_Layout/footer.php");
    }
}
?>
