<?php

class Webshop extends Controller {
    public static function getProducts() {
        $results = self::query("SELECT * FROM product");

        return $results;
    }

    public static function getProduct($id) {
        if (isset($id)) {
            $params = array(
                ':ID' => $id,
            );

            $results = self::query("SELECT * FROM product where ID = :ID", $params)[0];

            return $results;
        }
    }

    public static function deleteProduct($id) {
        if (isset($id)) {
            $params = array(
                ':ID' => $id,
            );

            $results = self::query("DELETE FROM product where ID = :ID", $params);

            $message = "Product has been deleted";
            header("Refresh: 1; url=/Webshop/Index");

            return $message;
        }
    }

    public static function setProduct($name, $description, $price, $image, $id = "") {
        if(!empty($name) && !empty($description) && !empty($price) && !empty($image)) {
            if($name === strip_tags($name) && $price === strip_tags($price) && $image === strip_tags($image)) {
                if (isset($id) && !empty($id)) {
                    $params = array(
                        ':name' => $name,
                        ':description' => $description,
                        ':price' => $price,
                        ':image' => $image,
                        ':id' => $id
                    );

                    $result = self::query("UPDATE product set name=:name, description=:description, price=:price, image=:image WHERE ID = :id", $params);
                    $message = 'Product has been updated';
                    header("Refresh: 1; url=/Webshop/Index");
                } else {
                    $params = array(
                        ':name' => $name,
                        ':description' => $description,
                        ':price' => $price,
                        ':image' => $image,
                    );

                    $result = self::query("INSERT INTO product (name, description, price, image) VALUES (:name, :description, :price, :image)", $params);
                    $message = 'New product has been created';
                    header("Refresh: 1; url=/Webshop/Index");
                }
            }
            else {
                $message = 'Thou scripting shall not pass!!!';
            }
        }
        else {
            $message = 'Please fill out all fields';
        }
        return $message;
    }
}
?>
