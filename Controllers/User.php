<?php
class User extends Controller {

    private static function passwordHash($pass) {
        $password = password_hash($pass, PASSWORD_BCRYPT);
        return $password;
    }

    public static function loggedIn() {
        if (isset($_SESSION['user'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function isAdmin() {
        if (self::loggedIn()) {
            if ($_SESSION['user']['roles_ID'] == 0 || $_SESSION['user']['roles_ID'] == 1) {
                return true;
            }
        } else {
            return false;
        }
    }

    public static function Logout() {
        session_unset();
        session_destroy();
        header("Refresh: 0; url=/Home/");
    }

    public static function redirectLoggedIn() {
        if(self::loggedIn()) {
            header("Refresh: 0; url=/Home/");
        }
    }

    public static function redirectNotLoggedIn() {
        if(!self::loggedIn()) {
            header("Refresh: 0; url=/User/Login");
        }
    }

    public static function getUserRoles() {
        $results = self::query("SELECT * FROM roles");

        return $results;
    }

    public static function Login($email, $pass) {
        if(!empty($email) && !empty($pass)) {
            $params = array(
                ":mail" => $email
            );
            $results = self::query("SELECT ID, first, middle, last, mail, password FROM users WHERE mail = :mail", $params);

            if(count($results) > 0 && password_verify($pass, $results[0]['password'])) {
                $_SESSION['user'] = self::getSafeUserData($results[0]['ID']);
                header("Refresh: 1; url=/User/");
                return $message = 'succesfully logged in';
            } else {
                throw new Exception("Could not login");
            }
        } else {
            throw new Exception("Please fill in all fields");
        }
    }

    public static function getSafeUserData($user) {
        $params = array(
            ":id" => $user
        );
        $results = self::query("SELECT ID, first, middle, last, mail, roles_ID FROM users WHERE ID = :id", $params)[0];

        return $results;
    }

    private function getAllCurrentUserData($user) {
        $params = array(
            ":id" => $user
        );
        $results = self::query("SELECT ID, first, middle, last, mail, roles_ID FROM users WHERE ID = :id", $params)[0];

        return $results;
    }

    // Only use for SuperAdmin role
    public static function getAllUsers() {
        $params = array(
            ":id" => $_SESSION['user']['ID']
        );
        $results = self::query("SELECT ID, first, middle, last, mail, roles_ID FROM users WHERE ID != :id", $params);

        return $results;
    }

    public static function register($first, $middle, $last, $email, $password, $repassword){
        if(!empty($first) && !empty($last) && !empty($email) && !empty($password) && !empty($repassword)){
            if($first === strip_tags($first) && $middle === strip_tags($middle) && $last === strip_tags($last)){
                if($email === filter_var($email, FILTER_VALIDATE_EMAIL)){
                    if($password === $repassword){
                        // Enter the new user in the database
                        $password = self::passwordHash($password);
                        $params = array(
                            ':first' => $first,
                            ':middle' => $middle,
                            ':last' => $last,
                            ':email' => $email,
                            ':pass' => $password,
                        );

                        $result = self::query("INSERT INTO users (first, middle, last, mail, password, roles_ID) VALUES (:first, :middle, :last, :email, :pass, 3)", $params);
                        $message = 'Your account has been created';
                        header("Refresh: 1; url=/User/Login");
                    }
                    else {
                        $message = 'The passwords do not match';
                    }
                }
                else {
                    $message = 'Invalid email entry';
                }
            }
            else {
                $message = 'Thou scripting shall not pass!!!';
            }
        }
        else {
            $message = 'Please fill out all fields';
        }
        return $message;
    }

    public static function setUser($userID, $first, $middle, $last, $mail, $role, $currpass, $newpass) {
        if (isset($userID) && isset($first) && isset($last)) {
            if ($first === strip_tags($first) && $middle === strip_tags($middle) && $last === strip_tags($last) && $currpass === strip_tags($currpass) && $newpass === strip_tags($newpass)) {
                if ($currpass != "" && $newpass != "") {
                    $params = array(
                        ":userId" => $userID
                    );
                    $userPass = self::query("SELECT `password` FROM `users` WHERE `ID`= :userId", $params);
                    echo print_r($userPass[0]['password'], true);
                    if(password_verify($currpass, $userPass[0]['password'])) {
                        $password = self::passwordHash($newpass);
                        $params = array(
                            ":userId" => $userID,
                            ":first" => $first,
                            ":middle" => $middle,
                            ":last" => $last,
                            ":mail" => $mail,
                            ":roles_ID" => $role,
                            ":newpass" => $password,
                        );
                        $results = self::query("UPDATE `users` SET `first`= :first, `middle`= :middle, `last`= :last, `mail`= :mail, `roles_ID`= :roles_ID,`password`= :newpass WHERE `ID`= :userId", $params);

                        $message = "";
                        header("Refresh: 0; url=/User/");
                    } else {
                        $message = "current password does not match";
                    }
                } else {
                    $params = array(
                        ":userId" => $userID,
                        ":first" => $first,
                        ":middle" => $middle,
                        ":last" => $last,
                        ":mail" => $mail,
                        ":roles_ID" => $role,
                    );
                    $results = self::query("UPDATE `users` SET `first`= :first, `middle`= :middle, `last`= :last, `mail`= :mail, `roles_ID`= :roles_ID WHERE `ID`= :userId", $params);

                    $message = "";
                    header("Refresh: 0; url=/User/");
                }
            } else {
                $message = 'Error, no HTML tags allowed!!';
            }
            return $message;
        }
    }

    public static function deleteUser($userID) {
        if (isset($userID)) {
            $params = array(
                ":userID" => $userID,
            );
            $results = self::query("DELETE FROM users WHERE ID = :userID", $params);

            $message = "The user has been deleted";
            header("Refresh: 1; url=/User/");
        }
    }
}
?>
