<?php
class Controller extends Database {
    public static function CreateView($controller, $viewName) {
        require_once("./Views/_Layout/menu.php");
        require_once("./Views/$controller/$viewName.php");
        require_once("./Views/_Layout/footer.php");
    }
}
?>
