<?php
class Route {
    public static $validRoute = array();

    public static function set($controller, $view, $function) {
        self::$validRoute[] = $controller;

        if (empty($_GET['controller']) && empty($_GET['view'])) {
            $_GET['controller'] = "Home";
            $_GET['view'] = "Index";
        } else if (!empty($_GET['controller']) && empty($_GET['view'])) {
            $_GET['view'] = "Index";
        }

        if ($_GET['controller'] == $controller && $_GET['view'] == $view) {
            $function->__invoke();
        }
    }
}
?>
