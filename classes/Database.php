<?php

class Database {

    private static function connect() {
        try {
            $conn = new PDO("mysql:host=".getenv('MYSQL_HOST').";dbname=".getenv('MYSQL_DATABASE')."", getenv('MYSQL_USER'), getenv('MYSQL_PASSWORD'));

            return $conn;
        } catch (PDOExeption $e) {
            die("connection failed: " . $e->getMessage());
        }
    }

    public static function query($query, $params = array()) {
      try {
        $statement = self::connect()->prepare($query);
        $statement->execute($params);

        if (explode(' ', $query)[0] == 'SELECT') {
            $data = $statement->fetchAll(PDO::FETCH_ASSOC);
            return $data;
        }
      } catch (PDOExeption $e) {
        return $e;
      }
    }
}
?>
