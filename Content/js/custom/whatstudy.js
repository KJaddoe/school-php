function scrollToLastMessage() {
    var chat = document.getElementById("chat");
    chat.scrollTop = chat.scrollHeight;
}

function getChatMessages(chatID) {
    $.ajax({
        type: "POST",
        url: "/Whatstudy/getChat",
        data: {
            id: chatID
        },
        success: function(data) {
            var chat = "";
            data.forEach(function (value, index) {
                chat+= '<li><div class="card"><div class="card-header"><h4>'+value.first+' '+((value.middle !== "") ? value.middle + ' ': "" ) + value.last +' ('+value.role+')</h4></div><div class="card-block"><p class="card-text">'+value.message+'</p></div><div class="card-footer text-muted"><p>'+value.dateSend+'</p></div></div></li>';
            });
            $('#chat').html(chat);
            scrollToLastMessage();
            $('#sendMessage textarea').val("");
        }
    });
    return;
}

function sendChatMessage(chatID, userID, message) {
    $.ajax({
        type: "POST",
        url: "/Whatstudy/sendChatMessage",
        data: {
            chatID: chatID,
            userID: userID,
            message: message
        },
        success: function(data) {
            getChatMessages(chatID);
        }
    });
    return;
}
