<?php
    Route::set('Home', 'Index', function() {
        Home::CreateView('Home', 'Index');
    });
    Route::set('User', 'Index', function() {
        User::redirectNotLoggedIn();
        $view = new View('User', 'Index');

        $view->assign('users', User::getAllUsers());
    });
    Route::set('User', 'Login', function() {
        User::redirectLoggedIn();
        User::CreateView('User', 'Login');
    });
    Route::set('User', 'Logout', function() {
        User::Logout();
    });
    Route::set('User', 'Register', function() {
        User::redirectLoggedIn();
        User::CreateView('User', 'Register');
    });
    Route::set('User', 'ChangeName', function() {
        User::redirectNotLoggedIn();
        User::CreateView('User', 'ChangeName');
    });
    Route::set('User', 'EditUser', function() {
        if (User::loggedIn() && !empty($_POST['ID'])) {
            $view = new View('User', 'EditUser');
            $view->assign('editUser', User::getSafeUserData($_POST['ID']));
            $view->assign('userRoles', User::getUserRoles());
            print_r($_POST);
            if (!empty($_POST['firstName'])  && !empty($_POST['lastName']) && !empty($_POST['email']) && !empty($_POST['role']) && empty($_POST['currpass']) && empty($_POST['newpass'])) {
                $view->assign('message', User::setUser($_POST['ID'], $_POST['firstName'], $_POST['preposition'], $_POST['lastName'], $_POST['email'], $_POST['role'], "", ""));
            } else if (!empty($_POST['firstName'])  && !empty($_POST['lastName']) && !empty($_POST['email']) && !empty($_POST['role']) && !empty($_POST['currpass']) && !empty($_POST['newpass'])) {
                $view->assign('message', User::setUser($_POST['ID'], $_POST['firstName'], $_POST['preposition'], $_POST['lastName'], $_POST['email'], $_POST['role'], $_POST['currpass'], $_POST['newpass']));
            }
            if (isset($_POST['deleteUser']) && User::isAdmin()) {
                User::deleteUser($user['ID']);
            }
        } else {
            header("Refresh: 0; url=/User/");
        }
    });

    Route::set('Assignment', 'Index', function() {
        $view = new View('Assignment', 'Index');
    });
    Route::set('Assignment', 'BMI', function() {
        $view = new View('Assignment', 'BMI');
    });
    Route::set('Assignment', 'Dice', function() {
        $view = new View('Assignment', 'Dice');
    });
    Route::set('Assignment', 'High-Low', function() {
        $view = new View('Assignment', 'High-Low');
    });
    Route::set('Assignment', 'Spreuken', function() {
        $view = new View('Assignment', 'Spreuken');
    });
    Route::set('Assignment', 'Temperature', function() {
        $view = new View('Assignment', 'Temperature');
    });
    Route::set('Assignment', 'Zin', function() {
        $view = new View('Assignment', 'Zin');
    });
    Route::set('Assignment', 'TicTacToe', function() {
        $view = new View('Assignment', 'TicTacToe');
    });
    Route::set('Assignment', 'UserOOP', function() {
        class fakeUser {
            private $name;
            private $email;

            public function getName() {
                return $this->name;
            }

            public function setName($name) {
                $this->name = $name;
            }

            public function getEmail() {
                return $this->email;
            }

            public function setEmail($email) {
                $this->email = $email;
            }
        }

        $view = new View('Assignment', 'UserOOP');
        $view->assign('fakeUser', new fakeUser());
    });

    Route::set('Webshop', 'Index', function() {
        $view = new View('Webshop', 'Index');
        $view->assign('products', Webshop::getProducts());
    });
    Route::set('Webshop', 'CreateProduct', function() {
        User::redirectNotLoggedIn();
        $view = new View('Webshop', 'CreateProduct');

        if (!empty($_POST['name'])  && !empty($_POST['description']) && !empty($_POST['price']) && !empty($_POST['image'])) {
            $view->assign('message', Webshop::setProduct($_POST['name'], $_POST['description'], $_POST['price'], $_POST['image']));
        }
    });
    Route::set('Webshop', 'EditProduct', function() {
        User::redirectNotLoggedIn();
        $view = new View('Webshop', 'EditProduct');

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $view->assign('product', Webshop::getProduct($_GET['id']));
            if (!empty($_POST['name'])  && !empty($_POST['description']) && !empty($_POST['price']) && !empty($_POST['image'])) {
                $view->assign('message', Webshop::setProduct($_POST['name'], $_POST['description'], $_POST['price'], $_POST['image'], $_GET['id']));
            }
        } else {
            header("Refresh: 0; url=/Webshop/");
        }

    });
    Route::set('Webshop', 'DeleteProduct', function() {
        User::redirectNotLoggedIn();
        if (isset($_GET['id']) && !empty($_GET['id']) && User::isAdmin()) {
            $view = new View('Webshop', 'DeleteProduct');
            $view->assign('message', Webshop::DeleteProduct($_GET['id']));
        } else {
            header("Refresh: 0; url=/Webshop/");
        }

    });
    Route::set('Webshop', 'Product', function() {
        $view = new View('Webshop', 'Product');

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $view->assign('product', Webshop::getProduct($_GET['id']));
            if (isset($_POST['deleteProduct'])) {
                Webshop::deleteProduct($product['ID']);
            }
        } else {
            header("Refresh: 0; url=/Webshop/");
        }
    });

    Route::set('Whatstudy', 'Index', function() {
        User::redirectNotLoggedIn();
        $view = new View('Whatstudy', 'Index');

        $view->assign('chatUser', Whatstudy::checkChatUser($_SESSION['user']['ID']));
        $view->assign('chatRoles', Whatstudy::getChatRoles());
        $view->assign('chats', Whatstudy::getUserChats($_SESSION['user']['ID']));
    });

    Route::set('Whatstudy', 'Register', function() {
        if (isset($_POST['user']) && isset($_POST['role'])) {
            header('Content-type: application/json');
            echo Whatstudy::registerChatUser($_POST['user'], $_POST['role']);
        } else {
            header("Refresh: 0; url=/Home/");
        }
    });

    Route::set('Whatstudy', 'Chat', function() {
        User::redirectNotLoggedIn();
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $view = new View('Whatstudy', 'Chat');
            $view->assign('chat', Whatstudy::getChat($_GET['id']));
        } else {
            header("Refresh: 0; url=/Whatstudy/");
        }
    });

    Route::set('Whatstudy', 'getChat', function() {
        User::redirectNotLoggedIn();
        header('Content-type: application/json');
        echo json_encode(Whatstudy::getChat($_POST['id']));
    });

    Route::set('Whatstudy', 'sendChatMessage', function() {
        User::redirectNotLoggedIn();
        if (isset($_POST['userID'], $_POST['chatID'], $_POST['message'])) {
            Whatstudy::sendChatMessage($_POST['userID'], $_POST['message'], $_POST['chatID']);
        }
    });

    Route::set('Whatstudy', 'createNewChat', function() {
        User::redirectNotLoggedIn();
        if (isset($_POST['user1'], $_POST['user2'])) {
            Whatstudy::createNewChat($_POST['user1'], $_POST['user2']);
        }
    });
?>
