<?php
    redirectLoggedIn();

    $user = new User();
    if (isset($_POST['submit-form'])) {
        $first = $_POST['first'];
        $middle = $_POST['middle'];
        $last = $_POST['last'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $repassword = $_POST['check_password'];

        $user->Register($first, $middle, $last, $email, $password, $repassword);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Register below</title>
    </head>
    <body id="registerPage">
    </body>
</html>
