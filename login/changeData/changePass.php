<?php
    redirectNotLoggedIn();

    $message = '';
    if(!empty($_POST['currPass']) && !empty($_POST['newPass']) ) {
        $message = $users->setUserPass($user['ID'], $_POST['currPass'], $_POST['newPass']);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Change password</title>
    </head>
    <body id="changePass">

        <?php if(!empty($message)):?>
            <h3><?= $message ?></h3>
        <?php endif;?>
        <h1>Change password</h1>

        <form action="" method="post">
            <input type="password" placeholder="Current password" name="currPass" required>
            <input type="password" placeholder="New password" name="newPass" required>

            <input type="submit" value="Submit">
        </form>
        <span>after changing your password you will be redirected to the user page</span>
    </body>
</html>
