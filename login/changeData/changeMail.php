<?php
    redirectNotLoggedIn();

    $message = '';
    if(!empty($_POST['currMail']) && !empty($_POST['newMail']) ) {
        $message = $users->setUserMail($user['ID'], $_POST['currMail'], $_POST['newMail']);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Change e-mail</title>
    </head>
    <body id="changeMail">

        <?php if(!empty($message)):?>
            <h3><?= $message ?></h3>
        <?php endif;?>
        <h1>Change e-mail</h1>

        <form action="" method="post">
            <input type="email" placeholder="Current email" name="currMail" required>
            <input type="email" placeholder="New email" name="newMail" required>

            <input type="submit" value="Submit">
        </form>
        <span>after changing your e-mail you will be redirected to the user page</span>
    </body>
</html>
