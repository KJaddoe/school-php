<?php if(!empty($message)):?>
    <p><?= $message ?></p>
<?php endif;?>
<center>
    <h1>Create product</h1>

    <form class="col-sm-6 col-sm-offset-3" action="" method="post" novalidate>
        <div class="form-group">
            <input class="form-control" type="text" placeholder="Enter the product name" name="name" required>
        </div>
        <div class="form-group">
            <textarea class="form-control tinymce" name="description" required></textarea>
        </div>
        <div class="form-group">
            <input class="form-control" type="number" step="any" min="0" max="10000000" placeholder="Enter the price of the product" name="price" required>
        </div>
        <div class="form-group">
            <input class="form-control" type="text" placeholder="the image url" name="image" required>
        </div>
        <input class="btn btn-info" type="submit" value="Submit" name="submit-form">
    </form>
</center>
