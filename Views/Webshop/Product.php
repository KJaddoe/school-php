<div>
    <div class="col-md-4">
        <img class="img-fluid" src="<?= $product['image'] ?>">
    </div>
    <div class="col-md-8">
        <h2><?= $product['name'] ?></h2>
        <h4>Price: <span>&euro; <?= number_format($product['price'], 2, ",","")?></span></h4>
        <?php
            if (User::isAdmin()) {
                echo "
                    <a class='btn btn-success' href='/Webshop/EditProduct/{$product['ID']}'>Edit product</a>
                    <a class='btn btn-danger' href='/Webshop/DeleteProduct/{$product['ID']}' onclick=\"return confirm('Are you sure you want to delete this product?');\">Delete product</a>
                ";
            }
        ?>
    </div>
</div>
<div style="margin-top: 50px">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"><a class="nav-link active" href="#service-one" data-toggle="tab" role="tab">Description</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane active" id="service-one">
            <section class="container product-info">
                <?= $product['description']?>
            </section>
        </div>
    </div>
</div>
