<h1>All products</h1>
<?php
    if (User::isAdmin()) {
        echo '
            <div class="row" style="margin-bottom: 15px">
                <div class="col-sm-12">
                    <a class="btn btn-success" href="/Webshop/CreateProduct">Add new product</a>
                </div>
            </div>
        ';
    }
?>
<div id="products" class="card-columns">
    <?php foreach ($products as $product): ?>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header"><h4><?= $product['name'] ?></h4></div>
            <div class="img border-top-0"><a href="/Webshop/Product/<?= $product['ID'] ?>"><img src="<?= $product['image']?>" class="mx-auto d-block" alt="<?= $product['name'] ?>"></a></div>
            <div class="card-body align-bottom">
                <h4>Price: <span>&euro; <?= number_format($product['price'], 2, ",","")?></span></h4>
                <center>
                    <a href="/Webshop/Product/<?= $product['ID'] ?>" class="btn btn-info">More info</a>
                </center>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
