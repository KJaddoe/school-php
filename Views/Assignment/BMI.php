<?php

$message = "";

if (isset($_POST['name']) && isset($_POST['length']) && isset($_POST['weight'])) {
    $name   = $_POST['name'];
    $length = $_POST['length'];
    $weight = $_POST['weight'];

    if ($name === strip_tags($name)) {
        $bmi    = round(bmiCalc($length, $weight), 2);

        if($bmi < 18) {
            $message = "$name, je bent te licht (je hebt een bmi van $bmi)";
        } else if($bmi > 25) {
            $message = "$name, je bent te zwaar (je hebt een bmi van $bmi)";
        } else {
            $message = "$name, je bent normaal (je hebt een bmi van $bmi)";
        }
    } else {
        $message = "vul een valide naam in!!!!!";
    }
}

//calculate bmi
function bmiCalc($length, $weight)   {
    $BMI = $weight / ($length * $length);

    return $BMI;
}

?>
<center><h1>BMI calculator</h1></center>
<div class="row">
    <form action="#" method="post" style="margin: 0 auto">
        <div class="form-group">
            <input class="form-control" type="text" name="name" placeholder="naam" required>
            <input class="form-control" type="number" name="age" max="150" placeholder="leeftijd" required>
            <input class="form-control" type="number" name="length" step="0.01" min="0.01" max="2.50" placeholder="lengte" required>
            <input class="form-control" type="number" name="weight" step="0.01" min="0"  max="200" placeholder="gewicht" required>
        </div>
        <div class="form-group">
            <center><input class="btn btn-primary" type="submit"><center>
        </div>
    </form>
</div>
<div class="row">
    <?php
        echo "<p>$message</p>";
    ?>
</div>
