<?php
    $spreuk = [
        'Zonder doel voor ogen maak je nooit een doelpunt.',
        'De waarheid over de kat hoort men van de muizen.',
        'Wie de mensen kent heeft verstand; wie zichzelf kent, is verlicht.',
        'Als je doet wat je deed, krijg je wat je kreeg.',
        'Het oog ziet enkel dat wat de geest bereid is om te zien.',
        'Kennis komt maar wijsheid blijft.',
        'Als je niets leert van het verleden, zul je in de toekomst telkens nieuwe lessen krijgen.',
        'We zijn allemaal amateurs, want het leven is te kort om professional te worden.',
        'Als je het antwoord niet wilt horen, moet je de vraag niet stellen.',
        'Je kunt beter niets doen, dan druk te zijn met niets.'
    ];

    $message = $spreuk[rand(0,3)];
?>

<center>
    <?php echo $message; ?>
</center>
