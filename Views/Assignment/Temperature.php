<?php

    // calculate cel
    function toCel($fahr) {
        $cel = ($fahr - 32) / 9 * 5;

        return $cel;
    }

    // calculate fahr
    function toFahr($cel) {
        $fahr = $cel / 5 * 9 + 32;

        return $fahr;
    }

    if (isset($_POST['submit']) && isset($_POST['temp']) && isset($_POST['type'])) {
        $temp = $_POST['temp'];
        if ($_POST['type'] == "f") {
            $fahr = toFahr($_POST['temp']);
            $message = "$temp degrees Celsius equals $fahr degrees Fahrenheit";
        } else if ($_POST['type'] == "c") {
            $cel = toCel($_POST['temp']);
            $message = "$temp degrees Fahrenheit equals $cel degrees Celsius";
        }
    }

?>
<form class="form-horizontal" method="post">
    <div class="form-group">
        <label class="control-label col-sm-2" for="temp">Temperatuur:</label>
        <div class="col-sm-10">
            <input type="number" step="any" class="form-control" id="temp" placeholder="Voer de temperatuur in" name="temp" required>
        </div>
    </div>
    <div class="form-group">
        <div class="radio col-sm-offset-2">
            <label><input type="radio" name="type" value="c" checked="" required>Naar Celcius</label>
        </div>
        <div class="radio col-sm-offset-2">
            <label><input type="radio" name="type" value="f" required>Naar Fahrenheit</label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-default">Converteren</button>
        </div>
    </div>
</form>

<?php if (isset($message)): ?>
    <p><?php echo $message?></p>
<?php endif; ?>
