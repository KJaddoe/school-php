<div class="p1">
    <h2>Assignments P1</h2>
    <div class="list-group">
        <a class="list-group-item" href="/Assignment/BMI">Body Mass Index</a>
        <a class="list-group-item" href="/Assignment/Spreuken">Spreuken</a>
        <a class="list-group-item" href="/Assignment/Zin">Zin</a>
        <a class="list-group-item" href="/Assignment/Dice">Dice</a>
        <a class="list-group-item" href="/Assignment/High-Low">High - Low</a>
        <a class="list-group-item" href="/Assignment/Temperature">Temperature</a>
        <a class="list-group-item" href="/Assignment/TicTacToe">Tic tac toe</a>
        <a class="list-group-item" href="/User/Login">Login</a>
    </div>
</div>
<div class="p3">
    <h2>Assignments P3</h2>
    <div class="list-group">
        <a class="list-group-item" href="/Assignment/UserOOP">User OOP</a>
        <a class="list-group-item" href="/Whatstudy/">Whatstudy (User must be logged in) (No API)</a>
        <a class="list-group-item" href="https://school-whatstudy.karanjaddoe.nl">Whatstudy (using API)</a>
    </div>
    <a href="https://gitlab.com/KJaddoe/school-whatstudy/repository/master/archive.zip" class="btn btn-info">Download latest whatstudy source (api version)<span class="glyphicon glyphicon-save"></span></a>
</div>
<div class="p4">
    <h2>Assignments P4</h2>
    <div class="list-group">
        <a class="list-group-item" href="https://s1114761-php.clow.nl/quiz">Quiz</a>
    </div>
</div>
<div class="wordpress">
    <h2>SB</h2>
    <div class="list-group">
        <a class="list-group-item" href="https://school-wordpress.karanjaddoe.nl">Wordpress</a>
    </div>
</div>
<hr>
<a href="https://gitlab.com/KJaddoe/school-php/repository/windesheim/archive.zip" class="btn btn-info">Download newest source <span class="glyphicon glyphicon-save"></span></a>
