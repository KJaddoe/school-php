<?php
$tictactoe = new TicTacToe();

$box = $_SESSION['game'];

$player = $_SESSION['player'];
$gameOver = $_SESSION['gameOver'];
$winner = $_SESSION['winner'];
$score = $_SESSION['score'];

echo $tictactoe->checkWinner();

if ($player == "computer" && $gameOver === false) {
    echo $tictactoe->computerTurn($player, $gameOver, $box);
} else {
    if (!empty($_POST)) {
        foreach ($box as $key=>$value) {
            if (isset($_POST['box'.$key])) {
                $box[$key] = $_POST['box'.$key];
            }
        }
        $_SESSION['game'] = $box;
        $tictactoe->changeTurn();
        header("Refresh: 0;");
        //echo "<script>window.location = window.location.href;</script>";
    }
}

if (isset($_POST['resetGame'])) {
    $tictactoe->resetGame();
    header("Refresh: 0;");
    //echo "<script>window.location = window.location.href;</script>";
} else if (isset($_POST['resetBoard'])) {
    $tictactoe->resetBoard();
}

?>
<div id="tictactoe">
    <div id="header">
      <h1>Tic Tac Toe</h1>
    </div>
    <div id="board">
        <form method="post">
            <button name="resetBoard" class="btn btn-default" type="submit">Play again!</button>
            <button name="resetGame" class="btn btn-danger" type="submit">Reset</button>
        </form>
        <form method="post">
            <table class="container-fluid">
                <tr>
                    <?php
                    if ($gameOver == false) {
                        if ($player = "human") {
                            for ($i = 0; $i <= 8; $i++) {
                                if ($box[$i] == -1) {
                                    echo  "
                                        <td>
                                            <button disabled class=\"btn btn-default\" id=\"box\"  name=\"submit\" value=\"box$i\">0</button>
                                        </td>";
                                    echo  "<input id=\"box\" type=\"hidden\" name=\"box$i\" value=\"$box[$i]\">";
                                }
                                if ($box[$i] == 1) {
                                    echo  "
                                        <td>
                                            <button disabled class=\"btn btn-default\" id=\"box\"  name=\"submit\" value=\"box$i\">X</button>
                                        </td>";
                                    echo  "<input id=\"box\" type=\"hidden\" name=\"box$i\" value=\"$box[$i]\">";
                                }
                                if ($box[$i]== 0) {
                                    echo  "
                                        <td>
                                            <button class=\"btn btn-default\" id=\"box\" type=\"submit\" name=\"box$i\"  value=\"1\">+</button>
                                        </td>";
                                }
                                if ($i==2 || $i==5 || $i==8) {
                                    echo "</tr>";
                                }
                                echo '</div>';
                            }
                        } else {
                            for ($i = 0; $i <= 8; $i++) {
                                if ($box[$i] == -1) {
                                    echo  "
                                        <td>
                                            <button disabled class=\"btn btn-default\" id=\"box\"  name=\"submit\" value=\"box$i\">0</button>
                                        </td>";
                                    echo  "<input id=\"box\" type=\"hidden\" name=\"box$i\" value=\"$box[$i]\">";
                                }
                                if ($box[$i] == 1) {
                                    echo  "
                                        <td>
                                            <button disabled class=\"btn btn-default\" id=\"box\"  name=\"submit\" value=\"box$i\">X</button>
                                        </td>";
                                    echo  "<input id=\"box\" type=\"hidden\" name=\"box$i\" value=\"$box[$i]\">";
                                }
                                if ($box[$i]== 0) {
                                    echo  "
                                        <td>
                                            <button disabled class=\"btn btn-default\" id=\"box\" type=\"submit\" name=\"box$i\"  value=\"1\">+</button>
                                        </td>";
                                }
                                if ($i==2||$i==5||$i==8) {
                                    echo "</tr>";
                                }
                                echo '</div>';
                            }
                        }
                    } else {
                        for ($i = 0; $i <= 8; $i++) {
                            if ($box[$i] == -1) {
                                echo  "
                                    <td>
                                        <button disabled class=\"btn btn-default\" id=\"box\"  name=\"submit\" value=\"box$i\">0</button>
                                    </td>";
                                echo  "<input id=\"box\" type=\"hidden\" name=\"box$i\" value=\"$box[$i]\">";
                            }
                            if ($box[$i] == 1) {
                                echo  "
                                    <td>
                                        <button disabled class=\"btn btn-default\" id=\"box\"  name=\"submit\" value=\"box$i\">X</button>
                                    </td>";
                                echo  "<input id=\"box\" type=\"hidden\" name=\"box$i\" value=\"$box[$i]\">";
                            }
                            if ($box[$i]== 0) {
                                echo  "
                                    <td>
                                        <button disabled class=\"btn btn-default\" id=\"box\" type=\"submit\" name=\"box$i\"  value=\"1\">+</button>
                                    </td>";
                            }
                            if ($i==2||$i==5||$i==8) {
                                echo "</tr>";
                            }
                            echo '</div>';
                        }
                    }

                    echo '<div id="winner">';
                    ?>
                    <?php
                    echo "Score speler: {$_SESSION['score']['human']} <br>";
                    echo "Score computer: {$_SESSION['score']['computer']} <br>";
                    if ($gameOver == false) {
                        echo "{$_SESSION['player']}, it's your turn";
                    } else {
                        if ($winner === "draw") {
                            echo "The game has resulted in a draw!";
                        } else if ($winner === "computer") {
                            echo "The game is over, $winner has won. Try again?";
                        } else if ($winner === "humane") {
                            echo "Congratulations, you have won!!!!";
                        }
                    }
                    ?>
                </div>
            </table>
        </form>
    </div>
</div>
