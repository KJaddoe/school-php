<?php
    //reset game
    if (isset($_POST['reset'])) {
        session_unset();
        session_destroy();
        echo "<script>window.location = window.location.href;</script>";
    }

    if (isset($_SESSION['number'])) {
        // doe niks
    } else {
        $_SESSION['number'] = rand(1,100);
    }

    if (isset($_SESSION['count'])) {
        // doe niks
    } else {
        $_SESSION['count'] = 0;
    }

    if (isset($_POST['number'])) {
        $number = $_SESSION['number'];
        $userNumber = $_POST['number'];

        if (isset($_SESSION['number'])) {
            $number = $_SESSION['number'];
        } else {
            $_SESSION['number'] = $number;
        }

        if ($userNumber > $number) {
            echo "$userNumber is above the generated number";
        } else if ($userNumber < $number) {
            echo "$userNumber is below the generated number";
        } else {
            echo "$userNumber is the correct number. the page will refresh and a new number will be generated";
            session_unset();
            session_destroy();
            echo "<script>setTimeout( function() { window.location = window.location.href;}, 3000);</script>";
        }

        $_SESSION['count']++;

    }
?>
<p>submit a number between 1 and 100 (including 1 and 100)</p>
<form method="post">
    <div class="form-group">
        <label class="control-label" for="number">Mijn getal:</label>
        <input class="form-controll" type="number" name="number" min="1" max="100">
    </div>
    <input type="submit" class="btn btn-default">
</form>
<form method="post">
    <input type="submit" class="btn btn-danger" name="reset" value="Reset">
</form>

<p><b>Aantal gespeelde beurten:</b><?php echo $_SESSION['count'] ?></p>
