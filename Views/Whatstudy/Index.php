<h1>Welcome, <?= $user['first'] ?></h1>
<?php if (empty($chatUser)): ?>
    <p>It seems you haven't registered for the whatstudy chat yet. To register, fill in the form below</p>
    <form id="chatRegister" method="POST">
        <select class="form-control" name="role">
            <?php foreach($chatRoles as $role):?>
                <option value="<?= $role['ID'] ?>"><?= $role['dsc'] ?></option>
            <?php endforeach;?>
        </select>
        <input type="hidden" value="<?= $user['ID']?>" name="user">
        <input type="submit" value="Submit"/>
    </form>
    <script>
    $('document').ready(function() {
        $("#chatRegister").submit(function(e) {
            e.preventDefault();
            var formData = $(this).serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

            $.ajax({
                type: "POST",
                url: "/Whatstudy/Register",
                data: formData,
                success: function(data) {
                    if ('success' in data) {
                        console.log(data);
                        alert("You have successfully registered");
                        window.location.reload(1);
                    } else {
                        alert(data.error);
                    }
                }
            });
        });
    });
    </script>
<?php else:?>
    <h2>Your chats:</h2>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal">Create new chat</button>
    <ul class="list-group">
    <?php foreach($chats as $chat):?>
        <li class="list-group-item">
            <a href="/Whatstudy/Chat/<?= $chat['chatID'] ?>"><?= $chat['chatnaam'] ?></a>
        </li>
    <?php endforeach;?>
    </ul>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Create new chat</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <form id="newChat" method="POST">
                <div class="form-group">
                    <input class="form-control" type="number" placeholder="ID of user that you want to chat with" name="user2">
                    <input type="hidden" value="<?= $user['ID']?>" name="user1">
                </div>
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" value="Submit"/>
                </div>
            </form>
            <script>
            $('document').ready(function() {
                $("#newChat").submit(function(e) {
                    e.preventDefault();
                    var formData = $(this).serializeArray().reduce(function(obj, item) {
                        obj[item.name] = item.value;
                        return obj;
                    }, {});

                    if (formData.user2 !== "") {
                        if (formData.user1 !== formData.user2) {
                            $.ajax({
                                type: "POST",
                                url: "/Whatstudy/createNewChat",
                                data: formData,
                                success: function(data) {
                                    alert("created a new chat");
                                    window.location.reload(1);
                                }
                            });
                        } else {
                            alert("you can't chat with yourself!!");
                        }
                    } else {
                        alert("you can't chat nobody!!");
                    }
                });
            });
            </script>
          </div>
        </div>
      </div>
    </div>
<?php endif;?>
