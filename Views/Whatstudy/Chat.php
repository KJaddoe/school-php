<??>
<div>
    <a class="btn btn-primary" href="/Whatstudy/">Back to chat overview</a>
</div>
<ul id="chat" class="list-unstyled"></ul>
<form id="sendMessage" method="POST">
    <textarea class="form-control" name="message"></textarea>
    <input type="hidden" value="<?= $user['ID']?>" name="userID">
    <input type="hidden" value="<?= $_GET['id'] ?>" name="chatID">
    <input class="btn btn-default" type="submit" value="Submit"/>
</form>
<script>
$(document).ready(function() {
    getChatMessages(<?= $_GET['id'] ?>);
    $('#sendMessage').submit(function(e) {
        e.preventDefault();
        var formData = $(this).serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        if (formData.message !== "") {
            sendChatMessage(formData.chatID, formData.userID, formData.message);
        } else {
            alert("cannot send an empty message!!!");
        }
    });
});
</script>
