<?php
    if (isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
    }
?>
<html>
    <head>
        <title>S1114761 | PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="/library/tinymce/tinymce.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- custom css file-->
        <link rel="stylesheet" href="/Content/css/app.min.css" type="text/css">
    </head>
    <body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/Home/">S1114761</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/Home/">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a id="assignments" class="nav-link dropdown-toggle" href="/Assignment/Index" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Assignments</a>
                    <div class="dropdown-menu" aria-labelledby="assignments">
                        <a class="dropdown-item" href="/Assignment/BMI">Body Mass Index</a>
                        <a class="dropdown-item" href="/Assignment/Spreuken">Spreuken</a>
                        <a class="dropdown-item" href="/Assignment/Zin">Zin</a>
                        <a class="dropdown-item" href="/Assignment/Dice">Dice</a>
                        <a class="dropdown-item" href="/Assignment/High-Low">High - Low</a>
                        <a class="dropdown-item" href="/Assignment/Temperature">Temperature</a>
                        <a class="dropdown-item" href="/Assignment/TicTacToe">Tic tac toe</a>
                        <a class="dropdown-item" href="/Assignment/UserOOP">User OOP</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/Webshop/">Webshop</a>
                </li>
                <li class="nav-item dropdown">
                    <a id="whatstudy" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Whatstudy</a>
                    <div class="dropdown-menu" aria-labelledby="whatstudy">
                      <a class="dropdown-item" href="/Whatstudy/">Whatstudy (No API)</a>
                      <a class="dropdown-item" href="https://school-whatstudy.karanjaddoe.nl">Whatstudy (API)</a>
                    </div>
                </li>
                <li class="nav-item">
                </li>
                <li class="nav-item dropdown">
                    <a id="repos" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Repositories</a>
                    <div class="dropdown-menu" aria-labelledby="assignments">
                      <a class="dropdown-item" href="https://gitlab.com/KJaddoe/school-php" target="_blank">Gitlab repo this site</a>
                      <a class="dropdown-item" href="https://gitlab.com/KJaddoe/school-whatstudy" target="_blank">Gitlab repo Whatstudy</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://school-wordpress.karanjaddoe.nl">Wordpress</a>
                </li>
            </ul>
            <ul class="navbar-nav navbar-right">
                <?php if(!isset($_SESSION['user'])) : // if user not logged in?>
                    <li id="loginButton" class="nav-item">
                        <span class="glyphicon glyphicon-log-in"></span>
                        <a class="nav-link" href="/User/Login">login</a>
                    </li>
                <?php endif;?>
                <?php if(isset($_SESSION['user'])) : // if user logged in?>
                    <li class="nav-item dropdown">
                        <a id="user" class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php
                                echo $user['first'] . " ";
                                if ($user['middle']) {
                                    echo $user['middle'] . " ";
                                }
                                echo $user['last'];
                            ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="user">
                            <a class="dropdown-item" href="/User/">Profile</a>
                        </div>
                    </li>
                    <li id="logoutButton" class="nav-item">
                        <span class="glyphicon glyphicon-log-out"></span>
                        <a class="nav-link" href="/User/Logout">logout</a>
                    </li>
                <?php endif;?>
            </ul>
        </div>
    </nav>
    <div class="jumbotron container">
