<?php
    if (!empty($_POST['first'])  && !empty($_POST['last']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['check_password'])) {
        $message = User::register($_POST['first'], $_POST['middle'], $_POST['last'], $_POST['email'], $_POST['password'], $_POST['check_password']);
    }
?>
<?php if(!empty($message)):?>
    <p><?= $message ?></p>
<?php endif;?>
<div id="registerPage">
    <center>
        <h1>Register below</h1>

        <form class="col-sm-6 col-sm-offset-3" action="" method="post">
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Enter your firstname" name="first" required>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Enter your preposition" name="middle">
            </div>
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Enter your lastname" name="last" required>
            </div>
            <div class="form-group">
                <input class="form-control" type="email" placeholder="Enter your email" name="email" required>
            </div>
            <div class="form-group">
                <input class="form-control" type="password" placeholder="Enter password" name="password" required>
            </div>
            <div class="form-group">
                <input class="form-control" type="password" placeholder="Re-type password" name="check_password" required>
            </div>

            <input class="btn btn-info" type="submit" value="Submit" name="submit-form">
        </form>
        <span class="col-sm-12"> or <a href="/User/Login">login here</a></span>
    </center>
</div>
