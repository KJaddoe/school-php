<center>
    <?php if(!empty($message)):?>
        <h3><?= $message ?></h3>
    <?php endif;?>
    <h1>Edit user data</h1>

    <div class="col-sm-6 col-sm-offset-3">
        <form id="editUser" action="" method="post">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="First name" name="firstName" required value="<?= $editUser['first']?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Preposition" name="preposition" value="<?= $editUser['middle']?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Last name" name="lastName" required value="<?= $editUser['last']?>">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" name="email" required value="<?= $editUser['mail']?>">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Current password" name="currpass">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="New password" name="newpass">
            </div>
            <div class="form-group">
            <?php
                if ($_SESSION['user']['roles_ID'] == 0 && $editUser['roles_ID'] != 0) {
                    echo "<select class=\"form-control\" name='role'>";
                    foreach ($userRoles as $role) {
                        if ($role['ID'] != 0) {
                            if ($role['ID'] == $editUser['roles_ID'] ) {
                                echo "<option value='{$role['ID']}' selected>{$role['name']}</option>";
                            } else {
                                echo "<option value='{$role['ID']}'>{$role['name']}</option>";
                            }
                        }
                    }
                    echo "</select>";
                } else if ($_SESSION['user']['roles_ID'] != 0 && $editUser['roles_ID'] != 0) {
                    echo "<input type='hidden' value='{$editUser['roles_ID']}' name='role' class=\"form-control\">";
                    echo "<select name='role' class=\"form-control\" disabled>";
                    foreach ($userRoles as $role) {
                        if ($role['ID'] == $editUser['roles_ID']) {
                            echo "<option value='{$role['ID']}' selected>{$role['name']}</option>";
                        }
                    }
                    echo "</select>";
                } else {
                    echo "<input type='hidden' value='{$editUser['roles_ID']}' name='role' class=\"form-control\">";
                    echo "<select name='role' class=\"form-control\" disabled>";
                    foreach ($userRoles as $role) {
                        if ($role['ID'] == $editUser['roles_ID']) {
                            echo "<option value='{$role['ID']}' selected>{$role['name']}</option>";
                        }
                    }
                    echo "</select>";
                }
            ?>
            </div>
            <div class="form-group">
                <input type="hidden" name="ID" value="<?= $editUser['ID']?>">
                <input type="submit" class="btn btn-info form-control" value="Submit">
            </div>
        </form>
        <form action='' method='post'>
            <?php
                if ($_SESSION['user']['roles_ID'] == 0 && $editUser['roles_ID'] != 0) {
                    echo "<input name='deleteUser' class='btn btn-danger form-control' type='submit' value='DELETE USER' onclick=\"return confirm('Are you sure you want to delete this user?');\">";
                } else if ($_SESSION['user']['ID'] == $editUser['ID'] && $_SESSION['user']['roles_ID'] != 0 && $_SESSION['user']['roles_ID'] != 1) {
                    echo "<input name='deleteUser' class='btn btn-danger form-control' type='submit' value='DELETE USER' onclick=\"return confirm('Are you sure you want to delete this user?');\">";
                }
            ?>
            <input type="hidden" name="ID" value="<?= $editUser['ID']?>">
        </form>
    </div>
    <span class="col-sm-12">after changing the user data you will be redirected to the user page</span>
</center>
