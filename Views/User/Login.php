<?php
      try {
        if (isset($_POST['email']) && isset($_POST['password'])) {
          $message = User::Login($_POST['email'], $_POST['password']);
        }
      } catch (Exception $e) {
        $message = $e->getMessage();
      }
?>
<?php if(!empty($message)):?>
    <p><?= $message ?></p>
<?php endif;?>
<div id="loginPage">
    <center>
        <h1>Please login</h1>

        <form class="col-sm-6 col-sm-offset-3" action="" method="post">
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Enter your email" name="email" required>
            </div>
            <div class="form-group">
                <input class="form-control" type="password" placeholder="Enter password" name="password" required>
            </div>

            <input class="btn btn-info" name="submit" type="submit" value="Submit">
        </form>
        <span class="col-sm-12"> or <a href="/User/Register">register here</a></span><br><br>
        <table>
            <thead>
                <th>Type</th>
                <th>Email</th>
                <th>Password</th>
            </thead>
            <tr>
                <td>Guest</td>
                <td>guest@guest.com</td>
                <td>guest</td>
            </tr>
        </table>
    <center>
</div>
