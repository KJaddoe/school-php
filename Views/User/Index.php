<div class="container">
    <div class="username">
        <h1>
            <?php
                echo $user['first'] . " ";
                if ($user['middle']) {
                    echo $user['middle'] . " ";
                }
                echo $user['last'];
            ?>
        </h1>
    </div>
    <div class="userData">
        <table>
            <tr>
                <td>Name:</td>
                <td>
                    <?php
                        echo $user['first'] . " ";
                        if ($user['middle']) {
                            echo $user['middle'] . " ";
                        }
                        echo $user['last'];
                    ?>
                </td>
            </tr>
            <tr>
                <td>User mail:</td>
                <td><?php echo $user['mail'] ?></td>
            </tr>
            <tr>
                <td>User password:</td>
                <td>**************</td>
            </tr>
        </table>
        <form method="post" action="/User/EditUser">
            <input type="hidden" name="ID" value="<?= $user['ID']?>">
            <input type="submit" class="btn btn-default" value="edit user" />
        </form>
    </div>
    <?php
    if ($user['roles_ID'] == 3) {
        echo '<div class="bs-callout bs-callout-success">
                <h4>You are a registered user.</h4>
                <p>You can only see your user profile.</p>
                <p>Wait for the verification of your account in order to change your user-settings</p>
              </div>';
    } else if ($user['roles_ID'] == 2) {
        echo '<div class="bs-callout bs-callout-success">
                <h4>You are logged in as the admin</h4>
                <p>This means that the SuperAdmin has given you the rights to change your user settings</p>
              </div>';
    } else if ($user['roles_ID'] == 1) {
        echo '<div class="bs-callout bs-callout-success">
                <h4>You are a Admin user.</h4>
                <p>You have almost all rights</p>
              </div>';
    } else if($user['roles_ID'] == 0) {
        echo '<div class="bs-callout bs-callout-success">
               <h4>You are logged in as the SuperAdmin</h4>
               <p>You can see all users</p>
             </div>';
        echo '<table class="table">
               <thead>
                 <tr>
                   <th>ID</th>
                   <th>First name</th>
                   <th>Preposition</th>
                   <th>Last name</th>
                   <th>Email</th>
                   <th>User role</th>
                 </tr>
               </thead>
               <tbody>';
        foreach ($users as $value) {
            echo "<tr>";
            foreach ($value as $info) {
                echo "<td>".$info."</td>";
            }
            if ($value['roles_ID'] != 0) {
                echo "<td>
                    <form method=\"post\" action=\"/User/EditUser\">
                        <input type=\"hidden\" name=\"ID\" value=\"{$value['ID']}\">
                        <input type=\"submit\" class='btn btn-default' value=\"edit user\" />
                    </form>
                    </td>";
            } else {
                echo "<td></td>";
            }
            echo "</tr>";
        }
        echo '</tbody></table>';
    }
    ?>
</div>
