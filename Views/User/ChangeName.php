<?php
    if (!empty($_POST['firstName'])  && !empty($_POST['lastName'])) {
        $message = User::setUserName($user['ID'], $_POST['firstName'], $_POST['preposition'], $_POST['lastName']);
    }
?>
<?php if(!empty($message)):?>
    <h3><?= $message ?></h3>
<?php endif;?>
<h1>Change username</h1>

<form action="" method="post">
    <input type="text" placeholder="First name" name="firstName" required value="<?= $user['first']?>">
    <input type="text" placeholder="Preposition" name="preposition" value="<?= $user['middle']?>">
    <input type="text" placeholder="Last name" name="lastName" required value="<?= $user['last']?>">

    <input type="submit" value="Submit">
</form>
<span>after changing your username you will be redirected to the user page</span>
