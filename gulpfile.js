(function() { 'use strict';

    var gulp = require('gulp');
    var rename = require('gulp-rename');
    var plumber = require('gulp-plumber');
    var concat = require('gulp-concat');
    var jshint = require('gulp-jshint');
    var uglify = require('gulp-uglify');
    var less = require('gulp-less');
    var cssmin = require('gulp-cssmin');

    gulp.task('watch', function() {
        gulp.watch('./Content/js/custom/**/*.js', ['js']);
        gulp.watch('./Content/**/*.less', ['css']);
    });

    gulp.task('js', function() {
        gulp.src(['./Content/js/custom/**/*.js'])
            .pipe(plumber())
            .pipe(jshint())
            .pipe(jshint.reporter('jshint-stylish'))
            .pipe(jshint.reporter('fail'))
            .pipe(concat('app.js'))
            .pipe(gulp.dest('Content/js/'))
            .pipe(uglify())
            .pipe(rename({ extname: '.min.js' }))
            .pipe(gulp.dest('Content/js/'))
            .on('error', function(err) {
            console.log(err);
            this.emit('end');
        });
    });

    gulp.task('css', function() {

        gulp.src(['./Content/less/index.less'])
            .pipe(plumber())
            .pipe(less())
            .pipe(rename({ basename: 'app' }))
            .pipe(gulp.dest('./Content/css/'))
            .pipe(cssmin())
            .pipe(rename({ extname: '.min.css' }))
            .pipe(gulp.dest('./Content/css/'))
            .on('error', function(err) {
            console.log(err);
            this.emit('end');
        });

    });

    gulp.task('default', ['js', 'css', 'watch']);

})();

